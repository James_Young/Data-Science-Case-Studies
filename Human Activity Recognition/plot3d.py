# Import packages
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from sklearn.neural_network import MLPClassifier
from sklearn import decomposition


from pandas.tools.plotting import parallel_coordinates

from os import listdir

# Variables
x_train_file = "X_train.txt"
y_train_file = "y_train.txt"

x_test_file = "X_test.txt"
y_test_file = "y_test.txt"

path_to_data = "UCI HAR Dataset" 
path_to_training_data = "/".join([path_to_data, "train"])
path_to_test_data = "/".join([path_to_data, "test"])

features_file = "features.txt"


# Load data into numpy arrays
X_train_np = np.loadtxt(path_to_training_data + "/" + x_train_file)
y_train_np = np.loadtxt(path_to_training_data + "/" + y_train_file)

X_test_np = np.loadtxt(path_to_test_data + "/" + x_test_file)
y_test_np = np.loadtxt(path_to_test_data + "/" + y_test_file)

# Load features list
features_df = pd.read_csv(path_to_data + "/" + features_file, sep = " ", index_col=0, header=None) 
features_df.columns = ["names"]

# Convert to pandas data frame
X_train_df = pd.DataFrame(X_train_np)
y_train_df = pd.DataFrame(y_train_np)
X_test_df = pd.DataFrame(X_test_np)
y_test_df = pd.DataFrame(y_test_np)


# Label Columns
#X_train_df.columns = list(features_df.names.values)
y_train_df.columns = ["labels"]

n_features = X_train_df.shape[1]

X_train = np.array(X_train_df)
y_train = np.array(y_train_df).ravel()

X_test = np.array(X_test_df)
y_test = np.array(y_test_df).ravel()

# clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(n_features,int(n_features/2)), random_state=1)
# clf.fit(X_train, y_train)         


pca = decomposition.PCA(n_components=10)
pca.fit(X_train)
X_reduced = pca.transform(X_train)

fig = plt.figure(figsize=(10,10))
ax = fig.add_subplot(111, projection='3d')

# ax.scatter(X_reduced.T[0], X_reduced.T[1], X_reduced.T[2], c=y_train)

# plt.show()

